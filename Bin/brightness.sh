#! /bin/bash

CURRENT=`xrandr --verbose | grep -m 1 -i brightness | cut -f2 -d ' '`
OUTPUT=`xrandr|grep ' connected'|grep 'primary'|cut -d' ' -f 1`
GAP=0.1
NEWVALUE=${CURRENT}

case $1 in
    up)
        NEWVALUE=`echo "${CURRENT}+${GAP}"|bc -l`
    ;;
    down)
        NEWVALUE=`echo "${CURRENT}-${GAP}"|bc -l`
    ;;
    reset)
        NEWVALUE=1
    ;;
    read)
        echo "${CURRENT}*100/1"|bc
        exit 0
    ;;
esac

xrandr --output ${OUTPUT} --brightness ${NEWVALUE}
