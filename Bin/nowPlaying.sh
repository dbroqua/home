#! /bin/bash

spotifyIsUp=`pidof spotify`
rhythmboxIsUp=`pidof rhythmbox`

if [ "${spotifyIsUp}" ]; then
  echo $(qdbus org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Metadata | grep "^xesam:\(title\|artist\):" | grep -o " .*" | column | sed "s/\t\+/:/g")
elif [ "${rhythmboxIsUp}" ]; then
  echo $(rhythmbox-client --print-playing-format "%ta - %tt (%at)")
else
  echo ""
fi
