#! /bin/bash

ls -1tr | grep -v files.txt > files.txt

mencoder -nosound -noskip -oac copy -ovc copy -o output.avi -mf fps=20 'mf://@files.txt'

ffmpeg -i output.avi -y -sameq -vf scale=1920:1440,crop=1920:1080 output-final.avi
