#! /bin/bash

service=""
spotifyIsUp=`pidof spotify`
rhythmboxIsUp=`pidof rhythmbox`

if [ "${spotifyIsUp}" ]; then
  echo "Spotify is up!"
  service="spotify"
elif [ "${rhythmboxIsUp}" ]; then
  echo "Rhythmbox is up!"
  service="rhythmbox"
else
  echo "All down…"
  exit -1
fi

qdbus org.mpris.MediaPlayer2.${service} /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.$1
