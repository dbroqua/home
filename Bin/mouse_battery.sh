#! /bin/bash

MOUSE_PATH=`qdbus --system org.freedesktop.UPower|grep mouse`
PERCENTAGE=`qdbus --system org.freedesktop.UPower ${MOUSE_PATH} org.freedesktop.UPower.Device.Percentage`

echo ${PERCENTAGE}%
