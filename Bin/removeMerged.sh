#! /bin/bash

# Checkout develop
git develop

if [ $? = 1 ] ; then
    git checkout dev
fi

# Get last changes
git pull

# Get list of available remote's branch
git fetch --prune origin

# List merged branch and remove it
git branch --merged | egrep -v "(^\*|master|dev)" |xargs git branch -d
