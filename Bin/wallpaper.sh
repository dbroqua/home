#! /bin/bash

# CONFIG
DIRECTORY="/home/dbroqua/Nextcloud/images/Wallpaper/Rotation" 



# SCRIPT START HERE
SELECTED=`find ${DIRECTORY} -type f | shuf -n 1`

feh --bg-fill ${SELECTED} 
