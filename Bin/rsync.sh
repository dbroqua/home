#!/bin/bash

TYPE='' # usb / sshfs
CMD=rsync
ARGS='-avz --delete
    --exclude /Nextcloud
    --exclude /.cache
    --exclude /Downloads
    --exclude /.thunderbird
    --exclude /Projects
    --exclude /Software
    --exclude /tmp
    --exclude /fonts
    --exclude /Music
    --exclude /node_modules
    --exclude /snap
    --exclude /.local/share/Trash
    --exclude /.npm
    --exclude /.thumbnails
    --exclude /Android
    --exclude /.AndroidStudio*
    --exclude /.android
    --exclude /.bundle/cache
    --exclude /.config/cache
    --exclude /.config/Code/Cache
    --exclude /.config/Code/CachedData
    --exclude /.config/Slack
    --exclude /.config/*/Cache
    --exclude /.config/chromium
    --exclude /.mozilla
    --exclude /.config/discord
    --exclude /.config/spotify
    --exclude /.crashlytics
    --exclude /.fastlane
    --exclude /.gradle
    --exclude ./Software
    --exclude /.local
    --exclude /.DataGrip*'
ORIG='/home/dbroqua/'
DEST=''
HOSTNAME=`hostname`


# USB
UUID='6e3f7ae3-8fa1-43f3-88c0-a12651519ffb'
MOUNT='/mnt/backup'
DEST_USB="/mnt/backup/${HOSTNAME}/dbroqua/"
# SSHFS
DEST_SSHFS="storage:Backup/${HOSTNAME}/dbroqua/"

echo `date` > ${ORIG}/lastBackup

while getopts t:h opt
do
	case $opt in
		t)
			case ${OPTARG} in
				usb)
					TYPE='usb'
					DEST=${DEST_USB}
				;;
				sshfs)
					TYPE='sshfs'
					DEST=${DEST_SSHFS}
				;;
				*)
					echo "usb / sshfs attendu"
					exit 1;
			esac
		;;
	esac
done

echo "Method: ${TYPE}" >> ${ORIG}/lastBackup

if [ "${TYPE}" != '' ] ; then
	case ${TYPE} in
		usb)
			sudo cryptsetup luksOpen /dev/sdb1 backup
			sudo mount /dev/mapper/backup ${MOUNT}
			echo "Go !"
			if [ $? == 0 ] ; then
				${CMD} ${ARGS} ${ORIG} ${DEST}
			fi
			sudo umount ${MOUNT}
			sudo cryptsetup luksClose backup
		;;
		sshfs)
			${CMD} ${ARGS} ${ORIG} ${DEST}
		;;
	esac
fi

