#! /bin/bash

if [ -z "$1" ]; then
  echo "Missing session name"
  exit 1
fi

TMUXSESSION=$1
EXISTS=`tmux ls|grep "${TMUXSESSION}:"`

if [ -z "${EXISTS}" ]; then
  tmux new -s ${TMUXSESSION}
else
  tmux attach-session -t ${TMUXSESSION}
fi
