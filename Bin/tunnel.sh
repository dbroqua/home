#! /bin/bash

LOCAL_PORT=6660
CLIENT=127.0.0.1
REMOTE="-"

while [ $# -gt 0 ]; do
    case "$1" in
        --local-port=*)
            LOCAL_PORT="${1#*=}"
        ;;
        --remote=*)
            REMOTE="${1#*=}"
        ;;
        --client=*)
            CLIENT="${1#*=}"
        ;;
        *)
            printf "Invalid parameter $1"
            exit 1
    esac
    shift
done

if [ "${REMOTE}" = "-" ] ; then
    printf "Invalid value for --remote="
    exit 1
fi

REMOTE_SERVER=`echo ${REMOTE}|cut -d: -f1`
REMOTE_PORT=`echo ${REMOTE}|cut -d: -f2`

ssh -L ${LOCAL_PORT}:${CLIENT}:${REMOTE_PORT} ${REMOTE_SERVER}
