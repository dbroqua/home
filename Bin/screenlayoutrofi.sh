#! /bin/bash

rofi_command="rofi"
mode="laptop"
### Options ###
laptop=" Laptop only"
home=" Wide screen"
homeoffice="  Dual screen"
# Variable passed to rofi
options="$laptop\n$home\n$homeoffice"

chosen="$(echo -e "$options" | $rofi_command -dmenu -selected-row 1)"

case $chosen in
    $latpop)
	mode="laptop"
	;;
    $home)
	mode="home"
	;;
    $homeoffice)
	mode="homeoffice"
	;;
esac

~/Bin/screenlayout.sh $mode
