#! /bin/bash

activeconfig='laptop'

if [ -f ~/.screenlayout.info ] ; then
    activeconfig=`cat ~/.screenlayout.info`
fi

nextconfig='laptop'

case $activeconfig in
    home)
        nextconfig='homeoffice'
        ;;
    homeoffice)
        nextconfig='laptop'
        ;;
    laptop)
        nextconfig='home'
        ;;
esac

if [ $1 ]; then
    if [ "$1" = "menu" ]; then
	chosen="$(echo -e "$options" | $rofi_command -dmenu -selected-row 1)"
	exit 0
    else
	nextconfig=$1
    fi
fi

echo ${nextconfig} > ~/.screenlayout.info

case $nextconfig in
    home)
        # Home
        xrandr --output DP-2-1 --primary --mode 2560x1080 --pos 0x0 --rotate normal \
            --output DP-2-2 --off \
            --output DP-2-3 --off \
            --output eDP-1 --off \
            --output HDMI-2 --off \
            --output HDMI-1 --off \
            --output DP-2 --off \
            --output DP-1 --off
        ;;
    homeoffice)
        # Home Office
        xrandr --output DP-2-1 --primary --mode 2560x1080 --pos 0x0 --rotate normal \
            --output DP-2-2 --mode 1920x1080 --pos 2560x0 --rotate normal \
#            --output DP-2-2 --mode 1920x1080 --pos 2560x0 --rotate left \
            --output DP-2-3 --off \
            --output eDP-1 --off \
            --output HDMI-2 --off \
            --output HDMI-1 --off \
            --output DP-2 --off \
            --output DP-1 --off
        ;;
    laptop)
        # Laptop mode
        xrandr --output DP-2-1 --off \
            --output DP-2-2 --off \
            --output DP-2-3 --off \
            --output eDP-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal \
            --output HDMI-2 --off \
            --output HDMI-1 --off \
            --output DP-2 --off \
            --output DP-1 --off
        ;;
esac

# Reset wallpaper
~/Bin/wallpaper.sh
