# tmux cheatsheet

start new:

    tmux

start new with session name:

    tmux new -s myname

attach:

    tmux a  #  (or at, or attach)

attach to named:

    tmux a -t myname

list sessions:

    tmux ls

kill session:

    tmux kill-session -t myname

In tmux, hit the prefix `ctrl+b` and then:

## Sessions

    :new<CR>  new session
    s  list sessions
    $  name session

## Windows (tabs)

    c           new window
    ,           name window
    w           list windows
    f           find window
    &           kill window
    .           move window - prompted for a new number
    :movew<CR>  move window to the next unused number

## Panes (splits)

    \  horizontal split
    -  vertical split
    
    o  swap panes
    q  show pane numbers
    x  kill pane
    ⍽  space - toggle between layouts
    z  maximize pane

## Misc

    d  detach
    t  big clock
    ?  list shortcuts
    :  prompt

Resources:

* [Based on henrik config](https://gist.github.com/henrik/1967800)
* [cheat sheet](http://cheat.errtheblog.com/s/tmux/)
