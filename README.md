# home
My GNU/Linux home config

## Requirements

* i3
* py3status
* rofi (and rofi-scripts)
* rxvt-unicode
* tmux
* vim (and vimrc from https://github.com/amix/vimrc.git)
* maim
* ffmpeg
* xss-lock
* feh
* compton
* scrot
* ruby-notify
* qdbus
* i3lock-fancy
* zsh
